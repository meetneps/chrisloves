<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chrisloves');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'rootasdf!@#$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z$|5*/gX#RCIe.OMV2|9 _3z)4>.xC ?n<hjLP<`]FBk!>uF Jl:X32B)}>JnM{?');
define('SECURE_AUTH_KEY',  '}.^4%U;Bw>ztUE<@7EPsQLo$sz:>gX6F/v_Y|P+{W>U#HB5w>ttJDHN!G3 [^0-G');
define('LOGGED_IN_KEY',    '42(P-y^;|zw{#QCJ:-xX?[jT{!YMcfBakux.T($CEYxGH0=AZUn4wR@p]sA+v*3j');
define('NONCE_KEY',        '6OV6:ZYx!6A*Add$:j>5-=+>IhMF.-S-bIH+6n.=sf`6&P`iQ/HjZn{2uyS%*y9+');
define('AUTH_SALT',        'la=Shr@@xz9Ajmt^5MZQG--S7Jkd2p%ZddQqvTOu`7x.!eAB p+:AC_)s~[4jq]h');
define('SECURE_AUTH_SALT', '5mS<I35P<z4Sr=b+u~d2*.uY]&dkLm?Gc5o%i]OQybrpX>y|]BR*<whfW?o5tO?a');
define('LOGGED_IN_SALT',   'o0$8Y){J@&jON$wU$gOD~f[!*2j]3RqgQ]B4t[D|)4Ic-nbQhit8l+RS`:f-Z~E,');
define('NONCE_SALT',       'JJkSVxb%>p$4>dSkf)C:.[H>LJyGO`m+m4bMT7oy_nD;0fpV,TKcbqk~z](=sT6^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
